FROM debian:stable-slim
ENV net=eth0
ENV vid=10
ENV ipaddr=10.1.1.1
ENV monit=1
ENV pass=mypass
ENV delay=2

COPY start.sh /
COPY vrrpd /usr/sbin/vrrpd
COPY atropos /usr/sbin/atropos
COPY dockerscripts /vrrpd/
RUN apt-get update && apt-get install -y --no-install-recommends net-tools procps busybox curl dumb-init \
    && apt-get auto-remove -y && apt-get clean autoclean \
    && rm -rf /var/lib/apt/lists/* && rm -Rf /tmp/* 

ENTRYPOINT ["/usr/bin/dumb-init", "--"]
CMD ["/start.sh"]  
