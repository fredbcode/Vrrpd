#!/bin/bash

# Clean up previous VRRPd files
rm -Rf /var/run/vrrpd* 2>/dev/null
busybox syslogd -O /proc/1/fd/1 &
syslog_pid=$!

# SIGTERM handler
term_handler() {
# Send SIGTERM to vrrpd
    pkill vrrpd
    # Wait for vrrpd to terminate
    while pidof vrrpd >/dev/null; do
        echo "SIGTERM received, stopping VRRPD, please wait..." >> /proc/1/fd/1
        sleep 1
    done
    echo "VRRPD stopped successfully" >> /proc/1/fd/1
    exit 0
}

# Trap SIGTERM signal
trap 'term_handler' SIGTERM

# Start vrrpd (it daemonizes itself)
vrrpd -i "$net" "$ipaddr" -v "$vid" -M "$monit" -a "pw/$pass" -d "$delay" -U /vrrpd/Master.sh -D /vrrpd/Backup.sh

# Keep the container running by monitoring vrrpd
while pidof vrrpd >/dev/null; do
    sleep 5
done